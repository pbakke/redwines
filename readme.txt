# README #

Uses R and RStudio for the .rmd file

### What is this repository for? ###

Academic Data Exloration of a red wine data set.

### How do I get set up? ###

See the .html file for the results of my Exploratory Data Analysis.

See the data set .csv file and also see the .html notes section for a reference to the original source file.

### Who do I talk to? ###

Peter Bakke  peter@bakke.com