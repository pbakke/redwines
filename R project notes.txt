
Wine Quality Red project:  February 2018 PB


why are some wines denser than water???? density > 1.0  ???  alcohol is less dense than water, soooo... what's going on? furher reearch? to do another time? etc.

Why the need to do this, tho? (maybe explain my rationale for NOT doing this (other students have broken into categories of bad, average and good, for example. Why the need?) in notes or something) Since the definition of bad, average and good wines is subjective, I want to avoid the subjective  mapping of the the 0..10 rating system into categories of bad, average and good. One way to create  more objective and yet useful category assignments is to simply divide the wines into quartiles by ratings. This yields 'quality' ratings of 'q1,' 'q2,' 'q3,' and 'q4.' Count of each is ..
nope, median does not work,... need to use mean which falls between 5 and 6, else q problems... so m1 & m2... still not sure need to do, tho. why not just use the 3..8 ratings??... or does it make sense to create categorical vars based on integer ratings? Any difference?   


idea from https://sriramjaju.github.io/2017-09-10-EDA-RedWine/
create a categorical variable:  
As we can see that there are 6 levels on quality in our dataset, I will create an new variable called �ratings� with levels �good� for quality 7 and 8, �average� for quality of 5 and 6 and �bad� for quality of 3 and 4.
bad, average, good ... 


Note: several of the attributes (measurement) may be correlated (acid has 2 measures...PH as well, etc), thus it makes sense to apply some sort of feature selection.

 $ fixed.acidity       : num  7.4 7.8 7.8 11.2 7.4 7.4 7.9 7.3 7.8 7.5 ...
 $ volatile.acidity    : num  0.7 0.88 0.76 0.28 0.7 0.66 0.6 0.65 0.58 0.5 ...
 $ citric.acid         : num  0 0 0.04 0.56 0 0 0.06 0 0.02 0.36 ...
 $ residual.sugar      : num  1.9 2.6 2.3 1.9 1.9 1.8 1.6 1.2 2 6.1 ...
 $ chlorides           : num  0.076 0.098 0.092 0.075 0.076 0.075 0.069 0.065 0.073 0.071 ...
 $ free.sulfur.dioxide : num  11 25 15 17 11 13 15 15 9 17 ...
 $ total.sulfur.dioxide: num  34 67 54 60 34 40 59 21 18 102 ...
 $ density             : num  0.998 0.997 0.997 0.998 0.998 ...
 $ pH                  : num  3.51 3.2 3.26 3.16 3.51 3.51 3.3 3.39 3.36 3.35 ...
 $ sulphates           : num  0.56 0.68 0.65 0.58 0.56 0.56 0.46 0.47 0.57 0.8 ...
 $ alcohol             : num  9.4 9.8 9.8 9.8 9.4 9.4 9.4 10 9.5 10.5 ...
 $ quality  

Checked for bimodailty and even normal distribution of skewed data using log10() on univaraites 

Note:
... bivariate... see if the 2 each of acidity and sulfur have high coefficients.... remove one of each, then??

Programming / plotting / graphing notes:

to create a MAIN title in a grid of 2 plots??

# Boxplot of MPG by Car Cylinders 
boxplot(mpg~cyl,data=mtcars, main="Car Milage Data", 
  	xlab="Number of Cylinders", ylab="Miles Per Gallon")


blank out x and y axis labels 

theme(axis.title.x=element_blank(),
axis.text.x=element_blank(),
axis.ticks.x=element_blank())

https://sriramjaju.github.io/2017-09-10-EDA-RedWine/

---
Nice plotting and analysis examples of the Quality Wine Red dataset

WQD.1 - Exploratory Data Analysis (EDA) and Data Pre-processing
https://onlinecourses.science.psu.edu/stat857/node/224

EDA
https://sriramjaju.github.io/2017-09-10-EDA-RedWine/

EDA
https://github.com/sagarnildass/Exploratory-Data-Analysis-of-Red-Wine-by-R---A-Udacity-Project/blob/master/redWineAnalysis.Rmd


---


```{r echo=FALSE, message=FALSE, warning=FALSE, Univariate_Plot_quality}

p_hist_qu <- ggplot(data=wqr, aes(x=fixed.quality)) +
  geom_histogram(fill='darkred') +
  coord_flip()

p_sb_qu <- ggplot(data=wqr, aes(x=1, y=quality)) +
  geom_boxplot(color='red') + 
  geom_point(alpha=.25, position = position_jitter(h = 0)) +
  theme(axis.title.y=element_blank())  

grid.arrange(p_hist_qu,p_sb_qu,ncol=2,top = "Quality (0 to 10)")



---









